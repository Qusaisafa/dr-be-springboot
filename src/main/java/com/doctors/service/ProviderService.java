package com.doctors.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.doctors.dto.AvailableRequestDto;
import com.doctors.dto.AvailableTimesDto;
import com.doctors.dto.ProviderDto;
import com.doctors.model.Available;
import com.doctors.model.Provider;
import com.doctors.repository.AvailableRepository;
import com.doctors.repository.ProviderRepository;

@Service
public class ProviderService {
	
	@Autowired
	private ProviderRepository providerRepository;
	
	@Autowired
	private AvailableRepository availableRepository;
	

	public List<ProviderDto> getProviders(){
		List<ProviderDto> result = new ArrayList<>();
		for(Provider e: providerRepository.findAll()) {
			result.add(new ProviderDto(e));
		}
		return result;
	}
	
	public ProviderDto getProvider(Long id) {
		return new ProviderDto(this.providerRepository.findOne(id));
		//TODO handle exception	
	}
	
	@Transactional
	public String createProvider(ProviderDto provider) {
		Provider entities = new Provider(provider);
		this.providerRepository.save(entities);
		return "success";
	}
	
	@Transactional
	public String setAvialbleTimes(Long id, AvailableRequestDto available) {
		try {
			Provider p = this.providerRepository.findOne(id);
	          
			Available newAvailable = new Available(new SimpleDateFormat("HH:mm").parse(available.getFromTime()),
			 new SimpleDateFormat("HH:mm").parse(available.getToTime()), p);
			p.setAvailable(newAvailable);
			this.providerRepository.save(p);
			return "success";

		}catch(Exception e) {
			//TODO
			return e.getMessage();
		}
	}
	
	/**
	 * TODO
	 * Check available
	 * Check appointments
	 * Check blockTimeDate
	 * @return
	 * @throws ParseException 
	 */
	
	
	public List<AvailableTimesDto> availableTimes(Long id, String d) throws ParseException {
		List<AvailableTimesDto> availableTimes = new ArrayList<>();
		Available a =this.availableRepository.findByProviderId(id);
		
		 Date date = new SimpleDateFormat("yyyy-MM-dd").parse(d);
		 SimpleDateFormat dfValue = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		 SimpleDateFormat dfTime = new SimpleDateFormat("HH:mm");
		 
		 Calendar combineDateFrom = combineDateTime(date, a.getFromTime());
		 Calendar combineDateTo = combineDateTime(date, a.getToTime());

		while(true) {
			if(a.getTimeSlot() == 0) {
				break;
			}
			combineDateFrom.add(Calendar.MINUTE, a.getTimeSlot());
			if(combineDateFrom.getTimeInMillis() > combineDateTo.getTimeInMillis()) {
				break;
			}
			availableTimes.add(new AvailableTimesDto(dfTime.format(combineDateFrom.getTime()), dfValue.format(combineDateFrom.getTime()), true));
		}
		
		return availableTimes;			
	}
	
	private Calendar combineDateTime(Date date, Date time)
	{
		Calendar calendarA = Calendar.getInstance();
		calendarA.setTime(date);
		Calendar calendarB = Calendar.getInstance();
		calendarB.setTime(time);
	 
		calendarA.set(Calendar.HOUR_OF_DAY, calendarB.get(Calendar.HOUR_OF_DAY));
		calendarA.set(Calendar.MINUTE, calendarB.get(Calendar.MINUTE));
		calendarA.set(Calendar.SECOND, calendarB.get(Calendar.SECOND));
		calendarA.set(Calendar.MILLISECOND, calendarB.get(Calendar.MILLISECOND));
	 
//		Date result = calendarA.getTime();
		return calendarA;
	}
	
//	public boolean isAvailable(Long id, Date date) {
//		
//		return this.availableRepository.isAvailable(id, date);
//	}
}
