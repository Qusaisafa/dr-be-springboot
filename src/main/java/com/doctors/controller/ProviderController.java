package com.doctors.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.doctors.dto.AvailableRequestDto;
import com.doctors.dto.AvailableTimesDto;
import com.doctors.dto.ProviderDto;
import com.doctors.service.ProviderService;


@RequestMapping(value="/api/provider")
@Controller
public class ProviderController {

	@Autowired
	private ProviderService providerService;
	
	@PostMapping
	public ResponseEntity<String> AddDoctor(@RequestBody ProviderDto providerDto){
		return ResponseEntity.ok().body(this.providerService.createProvider(providerDto));
	}
	
	@PostMapping("/{id}/available")
	public ResponseEntity<String> setAvailableDailyTime(@PathVariable("id") Long id, @RequestBody AvailableRequestDto availableDto){
		return ResponseEntity.ok().body(this.providerService.setAvialbleTimes(id, availableDto));
	}
	
	@GetMapping
	public ResponseEntity<List<ProviderDto>> getAllDoctors(){
		return ResponseEntity.ok().body(this.providerService.getProviders());
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ProviderDto> getProvider(@PathVariable("id") Long id){
		return ResponseEntity.ok().body(this.providerService.getProvider(id));
	}
	
	@GetMapping("/{id}/available")
	public ResponseEntity<List<AvailableTimesDto>> getProviderAvailableTimes(@PathVariable("id") Long id, @RequestParam(name="date", required = false) String date) throws ParseException{
		return ResponseEntity.ok().body(this.providerService.availableTimes(id, date));
	}
	
}
