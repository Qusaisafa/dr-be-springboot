package com.doctors.dto;

public class AvailableTimesDto {
	
	private String time;
	
	private String value;
	 
	private boolean available;
	
	public AvailableTimesDto() {
		
	}
	

	public AvailableTimesDto(String time, String value, boolean available) {
		super();
		this.time = time;
		this.value = value;
		this.available = available;
	}


	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
	
	

}
