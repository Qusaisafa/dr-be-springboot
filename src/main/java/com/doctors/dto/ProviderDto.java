package com.doctors.dto;

import com.doctors.model.Provider;

public class ProviderDto {
	
	private Long id;
	
	private String name;
	
	private String city;
	
	private String address;
	
	private String phone;
	
	private String speciality;
	
	private String fbUrl;
	
	private String imageUrl;
	
	public ProviderDto() {
		
				
	}

	public ProviderDto(Provider provider) {
		super();
		this.id = provider.getId();
		this.name = provider.getName();
		this.city = provider.getCity();
		this.address = provider.getAddress();
		this.phone = provider.getPhone();
		this.speciality = provider.getSpeciality();
		this.fbUrl = provider.getFbUrl();
		this.imageUrl = provider.getImageUrl();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSpeciality() {
		return speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}

	public String getFbUrl() {
		return fbUrl;
	}

	public void setFbUrl(String fbUrl) {
		this.fbUrl = fbUrl;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	
	
}
