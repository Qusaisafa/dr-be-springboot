package com.doctors.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

public class AvailableRequestDto {
	
	public AvailableRequestDto() {
		
	}
	
	public AvailableRequestDto(int timeSlot, String fromTime, String toTime) {
		this.timeSlot = timeSlot;
		this.fromTime = fromTime;
		this.toTime = toTime;
	}

	@JsonFormat(pattern="HH:mm")
	private String fromTime;
	
	@JsonFormat(pattern="HH:mm")
	private String toTime;
	
	private int timeSlot;

	public String getFromTime() {
		return fromTime;
	}

	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}

	public String getToTime() {
		return toTime;
	}

	public void setToTime(String toTime) {
		this.toTime = toTime;
	}

	public int getTimeSlot() {
		return timeSlot;
	}

	public void setTimeSlot(int timeSlot) {
		this.timeSlot = timeSlot;
	}

	
}
