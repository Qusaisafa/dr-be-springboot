package com.doctors.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.doctors.model.Available;

public interface AvailableRepository extends JpaRepository<Available, Long>{

//	@Query(value = "SELECT CASE WHEN count(u)> 0 then true else false end FROM public.available u where u.id=:id and u.from_time<=:time and u.to_Time>:time")
//	public boolean isAvailable(@Param("id") Long id, @Param("time")Date time);
	
	
	Available findByProviderId(Long id);
}
