package com.doctors.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.doctors.model.BlockTimeDate;

public interface BlockTimeDateRepository extends JpaRepository<BlockTimeDate, Long>{

}
