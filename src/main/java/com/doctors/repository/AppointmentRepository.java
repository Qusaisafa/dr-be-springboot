package com.doctors.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.doctors.model.Appointment;


public interface AppointmentRepository extends JpaRepository<Appointment, Long>{

}
