package com.doctors.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.doctors.model.Provider;

public interface ProviderRepository extends JpaRepository<Provider, Long>{

}
