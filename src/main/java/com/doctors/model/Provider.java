package com.doctors.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.doctors.dto.ProviderDto;

@Entity
public class Provider {
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String name;
	
	private String city;
	
	private String address;
	
	private String phone;
	
	private String speciality;
	
	private String fbUrl;
	
	private String imageUrl;
	
	public Provider() {
		
	}
	
	public Provider(ProviderDto provider) {
		this.id = provider.getId();
		this.name = provider.getName();
		this.city = provider.getCity();
		this.address = provider.getAddress();
		this.phone = provider.getPhone();
		this.speciality = provider.getSpeciality();
		this.fbUrl = provider.getFbUrl();
		this.imageUrl = provider.getImageUrl();
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "provider")
    private List<Appointment> appointment = new ArrayList<Appointment>();
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Available available;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "provider")
    private List<BlockTimeDate> blockTimeDate = new ArrayList<BlockTimeDate>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSpeciality() {
		return speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}

	public String getFbUrl() {
		return fbUrl;
	}

	public void setFbUrl(String fbUrl) {
		this.fbUrl = fbUrl;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public List<Appointment> getAppointment() {
		return appointment;
	}

	public void setAppointment(List<Appointment> appointment) {
		this.appointment = appointment;
	}

	public Available getAvailable() {
		return available;
	}

	public void setAvailable(Available available) {
		this.available = available;
	}

	public List<BlockTimeDate> getBlockTimeDate() {
		return blockTimeDate;
	}

	public void setBlockTimeDate(List<BlockTimeDate> blockTimeDate) {
		this.blockTimeDate = blockTimeDate;
	}
	
}
