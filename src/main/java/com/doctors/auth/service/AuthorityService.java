package com.doctors.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.doctors.auth.entity.Authority;
import com.doctors.auth.repository.AuthorityRepository;


@Service
public class AuthorityService {

	private AuthorityRepository authorityRepository;

	@Autowired
	public AuthorityService(AuthorityRepository authorityRepository) {
		this.authorityRepository = authorityRepository;	
	}

	@Transactional
	public void create(Authority authority) {
		this.authorityRepository.save(authority);
	}
}
