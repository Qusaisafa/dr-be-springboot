package com.doctors.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.doctors.auth.entity.Authority;
import com.doctors.auth.entity.AuthorityName;
import com.doctors.auth.entity.User;
import com.doctors.auth.repository.UserRepository;
import com.doctors.dto.UserDto;
import com.doctors.exception.UserNotFoundException;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private AuthorityService authorityService;
	
	public User findUserByUserName(String username) {
		return userRepository.findByUsername(username);
	}
	
	public User findUserByEmail(String email) {
		return userRepository.findByEmail(email);
	}
	
	public UserDto createUser(UserDto userDto) {
		User user = new User();
//		user.setName(userDto.getName());
		user.setEmail(userDto.getEmail());
		user.setPassword(this.passwordEncoder.encode(userDto.getPassword()));
		user.setSurname(userDto.getSurname());
		user.setUsername(userDto.getName());
		user.setEnabled(true);
		
		user = this.userRepository.save(user);
		Authority auth = new Authority();
		auth.setName(AuthorityName.ROLE_USER);
		auth.getUsers().add(user);
		user.getAuthorities().add(auth);
		authorityService.create(auth);
		
		return new UserDto(user);
	}
	
	
	public UserDto disableUser(String username) {
		User user= userRepository.findByUsername(username);
		if(user == null) {
			throw new UserNotFoundException(username);
		}
		
		user.setEnabled(false);
		
		user= userRepository.save(user);
		
		return new UserDto(user);
	}
}
