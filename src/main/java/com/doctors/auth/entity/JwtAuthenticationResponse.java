package com.doctors.auth.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * Created by stephan on 20.03.16.
 */
public class JwtAuthenticationResponse implements Serializable {
 
	private static final long serialVersionUID = -4199607766197824823L;

	private final String token;
    
    private final UserDetails user;
    
    private final List<String> role;

    public JwtAuthenticationResponse(String token,  List<String> role, UserDetails user) {
        this.token = token;
        this.role = role;
        this.user = user;
    }

    public String getToken() {
        return this.token;
    }
    
    public List<String> getRole() {
    		return this.role;
    }

	public UserDetails getUser() {
		return user;
	}
    
}