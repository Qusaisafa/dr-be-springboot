package com.doctors.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.doctors.auth.entity.JwtAuthenticationRequest;
import com.doctors.auth.entity.JwtAuthenticationResponse;
import com.doctors.auth.entity.User;
import com.doctors.auth.security.JwtTokenUtil;
import com.doctors.auth.security.JwtUser;
import com.doctors.auth.service.UserService;
import com.doctors.dto.UserDto;
import com.doctors.exception.UserNotFoundException;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class AuthenticationRestController {

	@Value("${jwt.header}")
	private String tokenHeader;

	private AuthenticationManager authenticationManager;

	private JwtTokenUtil jwtTokenUtil;

	private UserDetailsService userDetailsService;

	private UserService userService;

	
	
	@Autowired
	public AuthenticationRestController(AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil,
			UserDetailsService userDetailsService, UserService userService) {
		this.authenticationManager = authenticationManager;
		this.jwtTokenUtil = jwtTokenUtil;
		this.userDetailsService = userDetailsService;
		this.userService = userService;
	}

	@RequestMapping(value = "${jwt.route.authentication.path}", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest,
			Device device, HttpServletResponse response) throws AuthenticationException, IOException {
		System.out.println("createAuthenticationToken");
		System.out.println(authenticationRequest.getUsername());
		System.out.println(authenticationRequest.getPassword());
		User user = null;
		if(authenticationRequest.getUsername() != null) {
			user = userService.findUserByUserName(authenticationRequest.getUsername().toLowerCase());
		} else if(authenticationRequest.getEmail() != null) {
			user = userService.findUserByEmail(authenticationRequest.getEmail().toLowerCase());
		}
		if (user == null) throw new UserNotFoundException(authenticationRequest.getUsername());
		
		// Perform the security
		final Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername().toLowerCase(),
						authenticationRequest.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		// Reload password post-security so we can generate token
		final UserDetails userDetails = userDetailsService.loadUserByUsername(user.getUsername().toLowerCase());
		final String token = jwtTokenUtil.generateToken(userDetails, device);

		// Return the token
		return ResponseEntity.ok(new JwtAuthenticationResponse(token, new ArrayList(userDetails.getAuthorities().stream().map(p -> p.getAuthority()).collect(Collectors.toList())), userDetails));
	}

	@RequestMapping(value = "${jwt.route.authentication.refresh}", method = RequestMethod.GET)
	public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
		String token = request.getHeader(tokenHeader);
		String username = jwtTokenUtil.getUsernameFromToken(token);
		JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);

		if (jwtTokenUtil.canTokenBeRefreshed(token, user.getLastPasswordResetDate())) {
			String refreshedToken = jwtTokenUtil.refreshToken(token);
			return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken, null, null));
		} else {
			return ResponseEntity.badRequest().body(null);
		}
	}

	@PostMapping(value="/register")
    public ResponseEntity<UserDto> registerUser(@RequestBody UserDto userDto) {
		System.out.println("Registering user:");
		System.out.println(userDto);
		UserDto newUserDto = userService.createUser(userDto);
		return ResponseEntity.created(URI.create("/register")).body(newUserDto);
    }
	
	@GetMapping(value="/api/user")
    public ResponseEntity<UserDto> getCurrentUser(HttpServletRequest request) {
		String token = request.getHeader(tokenHeader);
		String username = jwtTokenUtil.getUsernameFromToken(token).toLowerCase();
		User user = userService.findUserByUserName(username);
		
		return ResponseEntity.ok().body(new UserDto(user));
	}
	
	@GetMapping(value="/api/user/disable/{username}")
    public ResponseEntity<UserDto> disableUser(@PathVariable("username") String username) {
		return ResponseEntity.ok().body(this.userService.disableUser(username));
	}

//	@PostMapping(value="/forgetpassword")
//	public ResponseEntity<?> processForgetPassword(@RequestBody UserDto userDto) {
////		SimpleMailMessage emailMessage = new SimpleMailMessage();
////		emailMessage.setFrom("support@shopperDiary.com");
//		otpService.sendOTP(userDto.getUsername().toLowerCase());
//		return ResponseEntity.ok().body("OTP send successfully");
//	}
//	
//	@PostMapping(value="/newpassword")
//	public ResponseEntity<?> setNewPassword(@RequestBody UserDto userDto) {
//		otpService.validateOTP(userDto.getUsername().toLowerCase(), userDto.getPassword(), userDto.getOtpCode());
//		return ResponseEntity.ok().body("password set successfully");
//	}
}