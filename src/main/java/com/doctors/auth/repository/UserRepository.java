package com.doctors.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.doctors.auth.entity.User;


/**
 * Created by stephan on 20.03.16.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findByEmail(String email);

}
