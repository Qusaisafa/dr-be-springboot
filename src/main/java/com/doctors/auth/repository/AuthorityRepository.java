package com.doctors.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.doctors.auth.entity.Authority;


public interface AuthorityRepository  extends JpaRepository<Authority , Long> {
}