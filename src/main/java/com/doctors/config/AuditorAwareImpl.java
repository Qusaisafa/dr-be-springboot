package com.doctors.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

import com.doctors.auth.entity.User;
import com.doctors.auth.service.UserService;

public class AuditorAwareImpl implements AuditorAware<User> {

	@Autowired
	private UserService userService;

	@Override
	public User getCurrentAuditor() {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		return userService.findUserByUserName(username);
		
	}
}
